(require '[clojure.string])
(def m (map
	#(Integer/parseInt %)
	(nth 1 (string/split (read-line) #" "))))

(def paths [])
(def queries [])
(def initial)
(def destination)

(for [i (range m)]
	(conj paths (zipmap [:u :v :w ] [
		(repeatedly 3
			(map #(Integer/parseInt %)
			(string/split (read-line) #" ")))])))

(let [uv (Integer/parseInt
(string/split (read-line) #" "))]
	(def initial (first uv))
	(def destination (last uv)))

(let [q (Integer/parseInt (read-line))]
	for([i (range q)]
		(conj queries 
			(Integer/parseInt (read-line)))))

(-> for [query queries]
	(filter #(
		(not= (query :v) (% :v))
		and (not= (query :u) (% :u)))

	(

